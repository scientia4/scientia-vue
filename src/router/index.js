import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Auth from '../views/Auth.vue';
import Topics from '../views/Topics.vue';
import Theory from '../views/Theory.vue';
import Test from '../views/Test.vue';
import TestEdit from '../views/TestEdit.vue';
import Profile from '../views/Profile.vue';
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },

  {
    path: '/auth',
    name: 'auth',
    component: Auth
  },
  {
    path: '/topics',
    name: 'topics',
    component: Topics
  },
  {
    path: '/test',
    name: 'test',
    component: Test
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile
  },
  // {
  //   path: '/about',  
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // },
  { path: '/theory/:id', name: 'theoryOfTopic', component: Theory },
  { path: '/test/:id', name: 'testOfTopic', component: Test },
  { path: '/test-list/:id', name: 'listOfTopicTests', component: TestEdit }


];

const router = new VueRouter({
  routes
});

export default router;
